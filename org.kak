provide-module orgmode %&
    # Define our highlighters in the shared namespace,
    # so we can link them later.
    add-highlighter shared/orgmode regions

# These are simple headers in the format of
    # * Title
add-highlighter shared/orgmode/headers region '^\*' '\n' group
add-highlighter shared/orgmode/headers/ fill header
# This allows archived headers
add-highlighter shared/orgmode/headers/ regex '.+:ARCHIVED:.+' 0:+di

# TODO and DONE
add-highlighter shared/orgmode/headers/ regex 'TODO' 0:red
add-highlighter shared/orgmode/headers/ regex 'DONE' 0:green

# Allow the priority to be set on a header `[1]` for example
    add-highlighter shared/orgmode/headers/priority regions
    add-highlighter shared/orgmode/headers/priority/ region '\[' '\]' fill +i

    # Tags
    add-highlighter shared/orgmode/headers/tags regions
    add-highlighter shared/orgmode/headers/tags/ region ':(?=[\S])' '(?<=[\S]):' fill meta

# Drawers, which should be hidden but aren't because kakoune does not
    # have code folding.
add-highlighter shared/orgmode/drawers region '^\h*:\w+:\h*$' '^\h*:END:\h*$' fill +d

    # Keywords
add-highlighter shared/orgmode/code region "#\+BEGIN_(\w+)$" "#\+END_\w+" fill "meta"
add-highlighter shared/orgmode/keywords region '#\+' '\n' fill "meta"

add-highlighter shared/orgmode/other default-region group

    # Bullets
    add-highlighter shared/orgmode/other/ regex "^[\s]*[-\+]" 0:bullet

    # Formatting

    # Bold
    add-highlighter shared/orgmode/other/bold regex "(?<=[\s])\*[\S][^$\*]*[\S]\*(?=[\s])" "0:+b"
    # Italics
    add-highlighter shared/orgmode/other/italics regex "(?<=[\s])/[\S][^$/]*[\S]/(?=[\s])" "0:+i"
    # Underlined
    add-highlighter shared/orgmode/other/underline regex "(?<=[\s])_[\S][^$_]*[\S]_(?=[\s])" "0:+u"
    # Strikethrough
    add-highlighter shared/orgmode/other/ regex "(?<=\s)\+[\S][^$\+]*[\S]\+(?=[\s])" "0:+d"
    # Inline Code
    add-highlighter shared/orgmode/other/ regex "(?<=\s)~[\S][^$~]*[\S]~(?=[\s])" "0:variable"

    # Checkboxes
    #
    # These use the terminal colour scheme which is less than ideal
    add-highlighter shared/orgmode/other/ regex "(?<=[-\+][\s])\[ \]" "0:red+d"
    add-highlighter shared/orgmode/other/ regex "(?<=[-\+][\s])\[-\]" "0:yellow"
    add-highlighter shared/orgmode/other/ regex "(?<=[-\+][\s])\[X\]" "0:green+d"
    add-highlighter shared/orgmode/other/ regex "\[[\d]*[%%/][\d]*\]" "0:+d"

    # Links
    add-highlighter shared/orgmode/links region '(?<!\\)\[(?<!\\)\[' '(?!\\)\](?!\\)\]' fill link

# Source code
evaluate-commands %sh{
  languages="
    awk c cabal clojure coffee cpp css cucumber d diff dockerfile fish
    gas go haml haskell html ini java javascript json julia kak kickstart
    latex lisp lua makefile markdown moon objc perl pug python ragel
    ruby rust sass scala scss sh swift toml tupfile typescript yaml sql
  "
  for lang in ${languages}; do
    printf "try 'require-module ${lang}'\n"
    printf "add-highlighter shared/orgmode/${lang} region \"(?<=#\+BEGIN_SRC ${lang}\\n)\" \"(?=#\+END_SRC)\" group\n"
    printf "add-highlighter shared/orgmode/${lang}/ fill code_block\n"
    ref=${lang}
    [ "${lang}" = kak ] && ref=kakrc || ref="${lang}"
    printf "add-highlighter shared/orgmode/${lang}/ ref ${ref}\n"
  done
}

&

# Keywords
evaluate-commands %sh{
    begin_blocks="BEGIN_SRC BEGIN_QUOTE"
    end_blocks=$(echo $begin_blocks | sed 's/BEGIN/END/g')

    join() { sep=$2; eval set -- $1; IFS="$sep"; echo "$*"; }
    printf %s\\n "declare-option str-list org_static_words $(join "${end_blocks} ${begin_blocks}" ' ')"
}

# When a window's `filetype` option is set to this filetype...
hook global WinSetOption filetype=org %{
    # Ensure our module is loaded, so our highlighters are available
    require-module orgmode
    hook window InsertChar \n -group org-indent org-indent-on-new-line
    hook window NormalIdle .* -group org-indent org-table-fix

    set-option window static_words %opt{org_static_words}

    # Link our higlighters from the shared namespace
    # into the window scope.
    add-highlighter window/orgmode ref orgmode
	map window normal <ret> ':org-toggle-check<ret>' -docstring 'Toggle Check'

    # Add a hook that will unlink our highlighters
    # if the `filetype` option changes again.
    hook -once -always window WinSetOption filetype=.* %{
        remove-highlighter window/orgmode
    }
}

# Lastly, when a buffer is created for a new or existing file,
# and the filename ends with `.org`...
hook global BufCreate .+\.org %{
    # ...we recognise that as our filetype,
    # so set the `filetype` option!
    set-option buffer filetype org
}

define-command -hidden org-indent-on-new-line %{
    evaluate-commands -itersel %{
        # preserve previous line indent
        try %{ execute-keys -draft <semicolon>K<a-&> }
	    # Indent after bullets
		try %{execute-keys -draft "kxs^[\s]*-<ret>ji -<esc>"}
		try %{execute-keys -draft "kxs^[\s]*\+<ret>ji +<esc>"}
		# BEGIN_BLOCKS
		try %{
            execute-keys -draft "kxs^\h*#\+BEGIN_\w+\h?[^\n]*$<ret>yjpxsBEGIN<ret>cEND<esc><lt>x<a-semicolon>3w<semicolon>lGl<a-d>xs\h+$<ret><a-d>"
            execute-keys "<esc>ko<esc><a-gt>i"
		}
     }
}

define-command org-toggle-check -docstring "Toggles a checkbox" %{
	evaluate-commands -draft %{
		try %{
			try %{
				try %{
					execute-keys "<a-x>s(?<=[\s]\[)[ -](?=\])"
					execute-keys "<ret>rX"
				} catch %{
					execute-keys "<a-x>s(?<=[\s]\[)[-X](?=\])"
					execute-keys "<ret>r "
				}
				try %{
					org-check-total
				}
			} catch %{
				try %{
					execute-keys "<a-x>s\**[\s]*TODO<ret>;BdiDONE"
				} catch %{
					execute-keys "<a-x>s\**[\s]*DONE<ret>;BdiTODO"
				}
			}
		} catch %{
			fail "No valid checkbox"
		}
	}
}

define-command org-check-total %{
	evaluate-commands -draft %{
    	# Fix indentation before headers.
		try %{ execute-keys -draft "<a-/>^\*+<ret>j<a-x>s^$<ret><a-gt>" }
		
		# Select everything on the current indentation, plus one up.
		execute-keys "<a-a>i<a-;>K"
		evaluate-commands -draft %sh{
    		# The head should be a checkbox on the level above this
			HEAD=$(echo "$kak_selection" | head -n 1)
			# This is the rest of the section, not including the checkbox
			MAIN_SEL=$(echo "$kak_selection" | tac | head -n -1 | tac)
			# This gets the level of indentation of the selection
			INDENTATION=$(echo "$MAIN_SEL" | head -n 1 | sed "s/-.*//g")
			# This gets all the lines that match the indentation. This should
			# be all the bullet points.
			INDENT_ONLY=$(echo "$MAIN_SEL" | grep "^$INDENTATION-")
			# This gets how many checkboxes there are.
			CHECK_TOTAL=$(echo "$INDENT_ONLY" | grep "\\([ ]\\|[X]\\)" | wc -l)
			# Find how many checked checkboxes there are.
			CHECKED=$(echo "$INDENT_ONLY" | grep "[X]" | wc -l)
			# Get the previous level of ticked.
            
            # Select the line which the toplevel is on.
            printf "execute-keys ';<a-x>'\n"
            # Selects the percentage or total in the form of:
            # - [%]
            # - [/]
            # - [10%]
            # - [1/10]
			echo "try %{
    			execute-keys 's\[[\d]*(/|%%)[\d]*\]'
    			execute-keys '<ret>d'
			}"
            
            # Works out which format it is in
			ISSLASH=$(echo "$HEAD" | grep -P "\[[\d]*/[\d]*\]")
			ISPERCENT=$(echo "$HEAD" | grep -P "\[[\d]*%\]")
			
			if [ ! -z "$ISSLASH" ]
			then
    			# If it is a slash then print in the form of ~[a/b]~
				printf "execute-keys 'i[${CHECKED}/${CHECK_TOTAL}]<esc>'\n"
			elif [ ! -z "$ISPERCENT" ]
			then
    			# If it is a percentage...
    			# Calculates the percentage to the nearest integer
				PERCENT=$(expr '(' $CHECKED '*' '100' ')' '/' $CHECK_TOTAL)
				# Format it as such (~[a%]~)
				printf "execute-keys 'i[${PERCENT}%%]<esc>'\n"
			fi
            
			if [ "$CHECKED" = "$CHECK_TOTAL" ]
			then
    			# If we have filled in everything then mark with an X.
    			# We then run this function recursively for the next level.
				echo 'try %{
					execute-keys "<a-x>s(?<=[\s]\[)[\h-](?=\])"
					execute-keys "<ret>rX"
					org-check-total
				}'
				# And try again for ~DONE~
				echo 'try %{
					execute-keys "<a-x>s(?<=\h)DONE(?=\h)"
					execute-keys "<ret>cDONE"
					org-check-total
				}'
			elif [ "$CHECKED" = 0 ]
			then
    			# If empty we will mark as such
    			echo 'try %{
					execute-keys "<a-x>s(?<=[\s]\[)[X-](?=\])"
					execute-keys "<ret>r "
					org-check-total
				}'
				# And try again for ~DONE~
				echo 'try %{
					execute-keys "<a-x>s(?<=\h)DONE(?=\h)"
					execute-keys "<ret>cTODO"
					org-check-total
				}'
			else
    			# Mark with a dash
				echo 'try %{
					execute-keys "<a-x>s(?<=[\s]\[)[X\h](?=\])"
					execute-keys "<ret>r-"
					org-check-total
				}'
				# And try again for ~DONE~
				echo 'try %{
					execute-keys "<a-x>s(?<=\h)DONE(?=\h)"
					execute-keys "<ret>cTODO"
					org-check-total
				}'
			fi
		}
		# Revert indentation before headers
		try %{ execute-keys -draft "<a-/>^\*+<ret>j<a-x>s^$<ret><a-lt>" }
	}
}

define-command org-table-fix %{
    try %{ execute-keys -draft '%s^#\+STARTUP:\h+align$<ret>:table-enable<ret>' }
    try %{ execute-keys -draft '%s^#\+STARTUP:\h+noalign$<ret>:table-disable<ret>' }
}
